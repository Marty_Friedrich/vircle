# Make your UI rounder with Vircle!

This plugin for Godot 4 adds a Container _Vircle_ to the Engine. It alligns all its children in a circle, but it can also do some other things:

- Adjust the size of all children elements at once

![Video of resizing](screenshots/change_size.webm)
- Adjust radius of the circle

![Video of radius adjustment](screenshots/change_radius.webm)
- Rotate elements around one another automatically (great for visuals)

![Video of rotating the Vircle](screenshots/auto_rotate.webm)

With this you can make all sorts of fun circular visuals!

![Example video of what is possible using Vircle and an AnimationPlayer Node](screenshots/animation_example.webm)

## Installation Instructions
1. Install plugin through the Asset Library or by cloning this repo into your project
2. Activate the plugin in the project settings

## Usage Instructions
1. Add the Vircle Node to your screenshots
2. Add as many controll children to this Vircle Node as you want. You can customize every Control individually, Vircle just serves as a container
3. Edit the properties of the Vircle Node to create your visuals

## Notes
I have noticed the Better Vircle plugin on the Asset Library which appears to be a copy of my project with a better folder structure. While I am thankful for now knowing that the folder structure of such plugins is very important, I wish I were informed in the form of a pull request of an issue instead. I have now edited the folder structure so that importing this addon is way easier than it was before.